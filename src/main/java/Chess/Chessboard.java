package Chess;

import Chess.Chessmen.Empty;

public class Chessboard {
    private final int height = 8;
    private final int width = 8;
    private Chessman[][] board;

    public Chessman[][] getBoard(){
        return board;
    }

    public Chessboard(Player black, Player white){
        board = new Chessman[height][width];

        board[0][0] = black.getChessman("rook1");
        board[0][1] = black.getChessman("knight1");
        board[0][2] = black.getChessman("bishop1");
        board[0][3] = black.getChessman("queen");
        board[0][4] = black.getChessman("king");
        board[0][5] = black.getChessman("bishop2");
        board[0][6] = black.getChessman("knight2");
        board[0][7] = black.getChessman("rook2");
        for (int i = 0; i < width; i++) {
            board[1][i] = black.getChessman("pawn" + (i + 1));
        }

        board[7][0] = white.getChessman("rook1");
        board[7][1] = white.getChessman("knight1");
        board[7][2] = white.getChessman("bishop1");
        board[7][3] = white.getChessman("queen");
        board[7][4] = white.getChessman("king");
        board[7][5] = white.getChessman("bishop2");
        board[7][6] = white.getChessman("knight2");
        board[7][7] = white.getChessman("rook2");
        for (int i = 0; i < width; i++) {
            board[6][i] = white.getChessman("pawn" + (i + 1));
        }

        for (int i = 2; i < height - 2; i++) {
            for (int j = 0; j < width; j++) {
                Empty empty = new Empty();
                board[i][j] = empty;
            }
        }
    }

    public void display(){
        System.out.print("  ");
        for (Chessman[] chessmanRow: board){

            System.out.print("-   ");
        }
        System.out.println("");
        for (Chessman[] chessmanRow: board){
            System.out.print("| ");
            for (Chessman chessman: chessmanRow){
                System.out.print(chessman.getSymbol() + " | ");
            }
            System.out.println("");
            System.out.print("  ");
            for (Chessman chessman: chessmanRow){
                System.out.print("-   ");
            }
            System.out.println("");
        }

    }
}
