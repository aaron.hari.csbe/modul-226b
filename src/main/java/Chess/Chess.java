package Chess;

public class Chess {
    private Player black;
    private Player white;
    private Chessboard chessboard;

    public Chess(){
        black = new Player();
        white = new Player();
        chessboard = new Chessboard(black, white);

    }

    public Player getBlack() {
        return black;
    }

    public Player getWhite() {
        return white;
    }

    public Chessboard getChessboard() {
        return chessboard;
    }

    public void setChessboard(Chessboard chessboard) {
        this.chessboard = chessboard;
    }
}
