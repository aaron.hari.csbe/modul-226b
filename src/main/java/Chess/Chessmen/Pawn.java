package Chess.Chessmen;

import Chess.Coordinate;

public class Pawn extends Chess.Chessman implements Chess.Chessmen.Chessman {

    public Pawn(){
        setName("Pawn");
        setSymbol('♙');
    }

    @Override
    public Coordinate[] getPossibleFields(Coordinate currentPosition) {
        return new Coordinate[0];
    }
}
