package Chess.Chessmen;

import Chess.Coordinate;

public interface Chessman {
    Coordinate[] getPossibleFields(Chess.Coordinate currentPosition);
}
