package Chess.Chessmen;

import Chess.Coordinate;

public class Knight extends Chess.Chessman implements Chess.Chessmen.Chessman {

    public Knight(){
        setName("Knight");
        setSymbol('♘');
    }

    @Override
    public Coordinate[] getPossibleFields(Coordinate currentPosition) {
        return new Coordinate[0];
    }
}
