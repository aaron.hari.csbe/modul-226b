package Chess.Chessmen;

import Chess.Coordinate;

public class King extends Chess.Chessman implements Chess.Chessmen.Chessman {

    public King(){
        setName("King");
        setSymbol('♔');
    }
    @Override
    public Coordinate[] getPossibleFields(Coordinate currentPosition) {
        return new Coordinate[0];
    }
}
