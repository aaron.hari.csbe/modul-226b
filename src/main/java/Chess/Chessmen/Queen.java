package Chess.Chessmen;

import Chess.Coordinate;

public class Queen extends Chess.Chessman implements Chess.Chessmen.Chessman {

    public Queen(){
        setName("Queen");
        setSymbol('♕');
    }

    @Override
    public Coordinate[] getPossibleFields(Coordinate currentPosition) {
        return new Coordinate[0];
    }
}
