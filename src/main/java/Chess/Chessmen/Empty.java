package Chess.Chessmen;

import Chess.Coordinate;

public class Empty extends Chess.Chessman implements Chess.Chessmen.Chessman {

    public Empty(){
        setName("Empty");
        setSymbol(' ');
    }

    @Override
    public Coordinate[] getPossibleFields(Coordinate currentPosition) {
        return new Coordinate[0];
    }
}
