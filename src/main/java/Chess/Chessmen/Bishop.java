package Chess.Chessmen;

import Chess.Chessman;
import Chess.Coordinate;

public class Bishop extends Chessman implements Chess.Chessmen.Chessman {

    public Bishop(){
        setName("Bishop");
        setSymbol('♗');
    }

    @Override
    public Coordinate[] getPossibleFields(Coordinate currentPosition) {
        return new Coordinate[0];
    }

}
