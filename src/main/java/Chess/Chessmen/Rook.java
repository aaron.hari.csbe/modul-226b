package Chess.Chessmen;

import Chess.Coordinate;

public class Rook extends Chess.Chessman implements Chess.Chessmen.Chessman {

    public Rook(){
        setName("Rook");
        setSymbol('♖');
    }

    @Override
    public Coordinate[] getPossibleFields(Coordinate currentPosition) {
        return new Coordinate[0];
    }
}
