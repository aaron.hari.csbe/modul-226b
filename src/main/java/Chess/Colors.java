package Chess;

/**
 * Eine Enumeration von Farben um die Konsolenausgabe zu gestallten.
 * Quelle: https://dev.to/awwsmm/coloured-terminal-output-with-java-57l3
 */
public enum Colors {
    Black("\u001B[30m"),
    Red("\u001B[31m"),
    Green("\u001B[32m"),
    Yellow("\u001B[33m"),
    Blue("\u001B[34m"),
    Purple("\u001B[35m"),
    Cyan("\u001B[36m"),
    White("\u001B[37m"),

    BlackBackground("\u001B[40m"),
    RedBackground("\u001B[41m"),
    GreenBackground("\u001B[42m"),
    YellowBackground("\u001B[43m"),
    BlueBackground("\u001B[44m"),
    PurpleBackground("\u001B[45m"),
    CyanBackground("\u001B[46m"),
    WhiteBackground("\u001B[47m");

    private String color;

    Colors(String s) {
        color = s;
    }

    String getColor() {
        return color;
    }
}

