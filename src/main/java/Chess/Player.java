package Chess;

import Chess.Chessmen.*;

public class Player {
    private Chessman[] chessmen = new Chessman[16];

    public Player(){
        King king = new King();
        Queen queen = new Queen();
        Rook rook1 = new Rook();
        Rook rook2 = new Rook();
        Bishop bishop1 = new Bishop();
        Bishop bishop2 = new Bishop();
        Knight knight1 = new Knight();
        Knight knight2 = new Knight();
        Pawn pawn1 = new Pawn();
        Pawn pawn2 = new Pawn();
        Pawn pawn3 = new Pawn();
        Pawn pawn4 = new Pawn();
        Pawn pawn5 = new Pawn();
        Pawn pawn6 = new Pawn();
        Pawn pawn7 = new Pawn();
        Pawn pawn8 = new Pawn();

        chessmen[0] = king;
        chessmen[1] = queen;
        chessmen[2] = rook1;
        chessmen[3] = rook2;
        chessmen[4] = bishop1;
        chessmen[5] = bishop2;
        chessmen[6] = knight1;
        chessmen[7] = knight2;
        chessmen[8] = pawn1;
        chessmen[9] = pawn2;
        chessmen[10] = pawn3;
        chessmen[11] = pawn4;
        chessmen[12] = pawn5;
        chessmen[13] = pawn6;
        chessmen[14] = pawn7;
        chessmen[15] = pawn8;

    }

    public Chessman getChessman(String chessmanName){
        Chessman chessmanToReturn;
        switch (chessmanName) {
            case "king":
                chessmanToReturn = chessmen[0];
                break;
            case "queen":
                chessmanToReturn = chessmen[1];
                break;
            case "rook1":
                chessmanToReturn = chessmen[2];
                break;
            case "rook2":
                chessmanToReturn = chessmen[3];
                break;
            case "bishop1":
                chessmanToReturn = chessmen[4];
                break;
            case "bishop2":
                chessmanToReturn = chessmen[5];
                break;
            case "knight1":
                chessmanToReturn = chessmen[6];
                break;
            case "knight2":
                chessmanToReturn = chessmen[7];
                break;
            case "pawn1":
                chessmanToReturn = chessmen[8];
                break;
            case "pawn2":
                chessmanToReturn = chessmen[9];
                break;
            case "pawn3":
                chessmanToReturn = chessmen[10];
                break;
            case "pawn4":
                chessmanToReturn = chessmen[11];
                break;
            case "pawn5":
                chessmanToReturn = chessmen[12];
                break;
            case "pawn6":
                chessmanToReturn = chessmen[13];
                break;
            case "pawn7":
                chessmanToReturn = chessmen[14];
                break;
            case "pawn8":
                chessmanToReturn = chessmen[15];
                break;
            default:
                chessmanToReturn = null;
        }
        return chessmanToReturn;
    }
}
