package repetition226a;

import java.util.ArrayList;

/**
 * Enhält eine Liste an Kunde einer Firma.
 */
public class Firma {
    //Liste von den Kunden
    ArrayList<Kunde> kunden = new ArrayList<Kunde>();

    /**
     * Gibt einen Kunden zurück anhand eines Indexes.
     * @param index position des Kundens in der Liste.
     * @return gibt den Kunden als Kunden zurück.
     */
    public Kunde getKunde(int index){
        return kunden.get(index);
    }

    /**
     * Um den Vor- und Nachnamen einses Kunden zu ändern.
     * @param index der Index des zuändernden Kunden.
     * @param newVorname der Neue Vorname.
     * @param newNachname der Neue Nachname.
     */
    public void setKunde(int index, String newVorname, String newNachname){
        kunden.get(index).vorname = newVorname;
        kunden.get(index).nachname = newNachname;
    }

    /**
     * Erstell einen neuen Kunden mit Vor- und Nachnamen und fügt diesen zur Liste der Kunden hinzu.
     * @param vorname der Voraname des neuen Kunden.
     * @param nachname der Nachname des neuen Kunden.
     */
    public void addNewKunde(String vorname, String nachname){
        kunden.add(new Kunde(vorname, nachname));
    }

    /**
     * Fügt einen bereits exsitierenden Kunden zur Liste kunden hinzu.
     * @param kunde der hinzu zufügende Kunde.
     */
    public void addKunde(Kunde kunde){
        kunden.add(kunde);
    }

    /**
     * Löscht einen Kunden aus der Liste.
     * @param nachname der Nachname des zulöschenden Kunden.
     */
    public void delKunde(String nachname){
        for (int i = 0; i < kunden.size(); i++)
            if (kunden.get(i).nachname == nachname) {
                kunden.remove(i);
            }
    }
}
