package repetition226a;

/**
 * Beinhaltet den Vor- und Nachnamen eines Kunden.
 */
public class Kunde {
    //Der Vorname eines Kunden.
    String vorname;
    //Der Nachname eines Kunden.
    String nachname;

    /**
     * Kunde mit Vor- und Nachname erstellen.
     * @param vorname der Vorname des Kunden.
     * @param nachname der Nachname des Kunden.
     */
    public Kunde(String vorname, String nachname){
        this.vorname = vorname;
        this.nachname = nachname;
    }

    /**
     * Kunden mit Nachnamen erstellen.
     * @param nachname der Nachname des Kunden.
     */
    public Kunde(String nachname){
        this.nachname = nachname;
    }
}
