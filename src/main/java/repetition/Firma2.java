package repetition;

import repetition226a.Kunde;

import java.util.ArrayList;

public class Firma2 {
    String firmenname;
    String strassenname;
    int strassennummer;
    ArrayList<Kunde2> kunden = new ArrayList<Kunde2>();


    public String getFirmenname() {
        return firmenname;
    }

    public void setFirmenname(String firmenname) {
        this.firmenname = firmenname;
    }

    public String getStrassenname() {
        return strassenname;
    }

    public void setStrassenname(String strassenname) {
        this.strassenname = strassenname;
    }

    public int getStrassennummer() {
        return strassennummer;
    }

    public void setStrassennummer(int strassennummer) {
        this.strassennummer = strassennummer;
    }

    public ArrayList<Kunde2> getKunden() {
        return kunden;
    }

    public void setKunden(ArrayList<Kunde2> kunden) {
        this.kunden = kunden;
    }

    public void addKunde(String vorname, String nachname){
        Kunde2 kunde = new Kunde2(vorname, nachname);
        System.out.println(kunde.getVorname() + " " + kunde.getNachname());
        ArrayList<Kunde2> kundenListe = this.getKunden();
        kundenListe.add(kunde);
        System.out.println("Fertig");
    }

    public void deleteKunde(int index){
        kunden.remove(index);
    }

    public void deleteKunde(String nachname){
        for (int i = 0; i < kunden.size(); i++){
            if (kunden.get(i).getNachname().equals(nachname)){
                kunden.remove(i);
            }
        }
    }
}
