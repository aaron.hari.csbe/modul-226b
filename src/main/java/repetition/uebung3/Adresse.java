package repetition.uebung3;

public class Adresse {
    private String strassenname;
    private String strassennumer;
    private String ort;
    private int postleitzahl;


    public boolean istAdressseVollstaendig(){
        boolean istAdressseVollstaendig = true;
        if (strassenname == null){
            istAdressseVollstaendig = false;
        }else if (strassennumer == null){
            istAdressseVollstaendig = false;
        }else if (ort == null){
            istAdressseVollstaendig = false;
        }else if (postleitzahl == 0){
            istAdressseVollstaendig = false;
        }
        return istAdressseVollstaendig;
    }

    public String getStrassenname() {
        return strassenname;
    }

    public void setStrassenname(String strassenname) {
        this.strassenname = strassenname;
    }

    public String getStrassennumer() {
        return strassennumer;
    }

    public void setStrassennumer(String strassennumer) {
        this.strassennumer = strassennumer;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public int getPostleitzahl() {
        return postleitzahl;
    }

    public void setPostleitzahl(int postleitzahl) {
        this.postleitzahl = postleitzahl;
    }
}
