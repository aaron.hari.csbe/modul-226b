package repetition.uebung3;

import java.util.ArrayList;
import java.util.Date;

public class Bestellung {
    private String datum;
    private ArrayList<BestellterArtikel> bestellterArtikel = new ArrayList<BestellterArtikel>();
    private ArrayList<Zahlungsmittel> zahlungen = new ArrayList<Zahlungsmittel>();

    public void addZahlung(Zahlungsmittel zahlung){
        this.zahlungen.add(zahlung);
    }

    public void addArtikel(BestellterArtikel bestellterArtikel){
        this.bestellterArtikel.add(bestellterArtikel);
    }

    public double gesamtPreis(){
        double gesamtPreis = 0;
        for (BestellterArtikel bestellterArtikel: bestellterArtikel) {
            gesamtPreis += (bestellterArtikel.getArtikel().getPreis() * bestellterArtikel.getAnzahl());
        }
        return gesamtPreis;
    }

    public int gesamtGewicht(){
        int gesamtGewicht = 0;
        for (BestellterArtikel bestellterArtikel: bestellterArtikel){
            gesamtGewicht += (bestellterArtikel.getArtikel().getGewicht() * bestellterArtikel.getAnzahl());
        }
        return gesamtGewicht;
    }

    public boolean istVollstaendigBezahlt(){
        double gesamtBezahlt = 0;
        boolean istVollstaendigBezahlt = false;
        for (Zahlungsmittel zahlungsmittel: zahlungen){
            gesamtBezahlt += zahlungsmittel.getBetrag();
        }
        if (gesamtBezahlt >= gesamtPreis()){
            istVollstaendigBezahlt = true;
        }
        return istVollstaendigBezahlt;
    }

    public double bereitsBezahlt(){
        double bereitsBezahlt = 0;
        for (Zahlungsmittel zahlungsmittel: zahlungen){
            bereitsBezahlt += zahlungsmittel.getBetrag();
        }
        return bereitsBezahlt;
    }

    public double offenerBetrag(){
        return (gesamtPreis() - bereitsBezahlt());
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public ArrayList<BestellterArtikel> getBestellterArtikel() {
        return bestellterArtikel;
    }

    public void setBestellterArtikel(ArrayList<BestellterArtikel> bestellterArtikel) {
        this.bestellterArtikel = bestellterArtikel;
    }

    public ArrayList<Zahlungsmittel> getZahlungen() {
        return zahlungen;
    }

    public void setZahlungen(ArrayList<Zahlungsmittel> zahlungen) {
        this.zahlungen = zahlungen;
    }
}
