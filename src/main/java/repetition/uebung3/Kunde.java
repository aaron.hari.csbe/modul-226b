package repetition.uebung3;

import java.util.ArrayList;

public class Kunde {
    private String vorname;
    private String nachname;
    private int alter;
    private String geschlecht;
    private ArrayList<Bestellung> bestellungen = new ArrayList<Bestellung>();
    private Adresse adresse;


    public void addBestellung(Bestellung bestellung){
        this.bestellungen.add(bestellung);
    }

    public void zeigeBezahlteBestellungen() {
        int index = 0;
        for (Bestellung bestellung : bestellungen) {
            if (bestellung.istVollstaendigBezahlt()) {
                zeigeBestellung(index);
            }
            index++;
        }
    }

    public void zeigeLetzteBestellung() {
        int indexLetzteBestellung = (bestellungen.size() - 1);
        zeigeBestellung(indexLetzteBestellung);
    }

    public void zeigeOffeneBetraege(){
        int index = 0;
        double offenerBetragTotal = 0;
        for (Bestellung bestellung: bestellungen){
            if (!bestellung.istVollstaendigBezahlt()){
                zeigeBestellung(index);
                offenerBetragTotal += bestellung.offenerBetrag();
                System.out.println("");
            }
            index++;
        }
        System.out.println("Offener Betrag Total: " + offenerBetragTotal);
    }

    public void zeigeBestellung(int index){
        Bestellung bestellung = bestellungen.get(index);
        if (bestellung.istVollstaendigBezahlt()){
            System.out.println("Ist vollständig Bezahlt");
        }else{
            System.out.println(bestellung.bereitsBezahlt() + " von " + bestellung.gesamtPreis() + " sind bereits Bezahlt");
            System.out.println("Offener Betrag: " + bestellung.offenerBetrag());
        }
        System.out.println("Datum: " + bestellung.getDatum());
        for (BestellterArtikel artikel : bestellung.getBestellterArtikel()) {
            System.out.println("Artikel: " + artikel.getArtikel().getName() + ", Gewicht: " + artikel.getArtikel().getGewicht() + ", Preis pro Stück: " + artikel.getArtikel().getPreis() + ", Anzahl: " + artikel.getAnzahl() + ", Gewicht Totoal: " + (artikel.getArtikel().getGewicht() * artikel.getAnzahl() + ", Preis Total: " + (artikel.getArtikel().getPreis() * artikel.getAnzahl())));
        }
        System.out.println("Gesamt Gewicht: " + bestellung.gesamtGewicht());
        System.out.println("Preis Total: " + bestellung.gesamtPreis());
    }

    public void erstelleEtikete(){
        System.out.println(vorname + " " + nachname);
        System.out.println(adresse.getStrassenname() + " " + adresse.getStrassennumer());
        System.out.println(adresse.getPostleitzahl() + " " + adresse.getOrt());
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public int getAlter() {
        return alter;
    }

    public void setAlter(int alter) {
        this.alter = alter;
    }

    public String getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(String geschlecht) {
        this.geschlecht = geschlecht;
    }

    public ArrayList<Bestellung> getBestellungen() {
        return bestellungen;
    }

    public void setBestellungen(ArrayList<Bestellung> bestellungen) {
        this.bestellungen = bestellungen;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }
}
