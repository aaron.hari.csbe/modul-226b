package repetition.uebung3;

public class Zahlungsmittel {
    private String name;
    private double betrag;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBetrag() {
        return betrag;
    }

    public void setBetrag(double betrag) {
        this.betrag = betrag;
    }


}
