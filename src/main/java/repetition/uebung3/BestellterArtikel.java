package repetition.uebung3;

import java.util.ArrayList;

public class BestellterArtikel {
    private Artikel artikel;
    private int anzahl;

    public Artikel getArtikel() {
        return artikel;
    }

    public void setArtikel(Artikel artikel) {
        this.artikel = artikel;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }
}
