package repetition.bauerhof;

import java.util.ArrayList;

public class Stall {
    private String name;
    private ArrayList<Tier> tiere;


    public Stall(String name){
        this.name = name;
    }

    public void getLaute(){
        for (Tier tier: tiere){
            System.out.println(tier.getLaut());
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Tier> getTiere() {
        return tiere;
    }

    public void addTier(Tier tier) {
        tiere.add(tier);
    }


}
