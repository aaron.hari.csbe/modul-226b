package repetition.bauerhof;

public class Huhn extends Vogel {
    private int rangInHackordung;

    public int getRangInHackordung() {
        return rangInHackordung;
    }

    public void setRangInHackordung(int rangInHackordung) {
        this.rangInHackordung = rangInHackordung;
    }
}
