package repetition.bauerhof;

public class Saeugetier extends Tier {
    private int anzahlMilchdrüssen;

    public int getAnzahlMilchdrüssen() {
        return anzahlMilchdrüssen;
    }

    public void setAnzahlMilchdrüssen(int anzahlMilchdrüssen) {
        this.anzahlMilchdrüssen = anzahlMilchdrüssen;
    }
}
