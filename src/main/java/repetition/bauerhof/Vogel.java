package repetition.bauerhof;

public class Vogel extends Tier{
    private int gewichtEier;

    public int getGewichtEier() {
        return gewichtEier;
    }

    public void setGewichtEier(int gewichtEier) {
        this.gewichtEier = gewichtEier;
    }
}
