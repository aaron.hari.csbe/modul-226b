package repetition.bauerhof;

public class Pferd extends Saeugetier{
    private String farbeMaehne;

    public String getFarbeMaehne() {
        return farbeMaehne;
    }

    public void setFarbeMaehne(String farbeMaehne) {
        this.farbeMaehne = farbeMaehne;
    }
}
