package repetition.bauerhof;

public class Kuh extends Saeugetier{
    private int groesseHoerner;

    public int getGroesseHoerner() {
        return groesseHoerner;
    }

    public void setGroesseHoerner(int groesseHoerner) {
        this.groesseHoerner = groesseHoerner;
    }
}
