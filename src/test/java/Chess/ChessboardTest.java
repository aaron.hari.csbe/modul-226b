package Chess;

import Chess.Chessmen.Empty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChessboardTest {

    Chess chess;

    @BeforeEach
    void setUp(){
        chess = new Chess();
    }

    @Test
    void display() {
        chess.getChessboard().display();
    }

    @Test
    void initialWhiteQueen(){
        assertEquals(chess.getWhite().getChessman("queen"), chess.getChessboard().getBoard()[7][3]);
    }

    @Test
    void initialBlackKing(){
        assertEquals(chess.getBlack().getChessman("king"), chess.getChessboard().getBoard()[0][4]);
    }

    @Test
    void initialWhitePawns(){
        for (int i = 0; i < 8; i++) {
            assertEquals(chess.getWhite().getChessman("pawn" + (i + 1)), chess.getChessboard().getBoard()[6][i]);
        }
    }

    @Test
    void initialEmptyFields(){
        for (int i = 2; i < 6; i++) {
            for (int j = 0; j < 8; j++) {
                assertEquals("Empty", chess.getChessboard().getBoard()[i][j].getName());
            }
        }

    }
}