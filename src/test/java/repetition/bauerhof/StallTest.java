package repetition.bauerhof;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class StallTest {
    Stall stall;
    Huhn huhn;
    Kuh kuh;
    Pferd pferd;

    @BeforeEach
    void setUp() {
        stall = new Stall("1");


        //Tiere
        huhn = new Huhn();
        huhn.setRangInHackordung(2);
        huhn.setAnzahlBeine(2);
        huhn.setGewicht(8);
        huhn.setGewichtEier(200);
        huhn.setLaut("Kiker iki");
        huhn.setName("Rocky");

        kuh = new Kuh();
        kuh.setGroesseHoerner(320);
        kuh.setAnzahlBeine(4);
        kuh.setAnzahlMilchdrüssen(4);
        kuh.setGewicht(4000);
        kuh.setLaut("Muh");
        kuh.setName("Milky");

        pferd = new Pferd();
        pferd.setFarbeMaehne("Braun");
        pferd.setAnzahlBeine(4);
        pferd.setAnzahlMilchdrüssen(6);
        pferd.setGewicht(3000);
        pferd.setLaut("Pffff");
        pferd.setName("Jolly Jumper");
    }

    @Test
    void getLaute() {
        stall.addTier(huhn);
        stall.addTier(kuh);
        stall.addTier(pferd);
        stall.getLaute();
    }

    @Test
    void addTier() {
        stall.addTier(huhn);
        stall.addTier(kuh);
        stall.addTier(pferd);
    }


}