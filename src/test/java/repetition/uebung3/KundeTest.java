package repetition.uebung3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class KundeTest {
    Kunde kunde = new Kunde();

    @BeforeEach
    void setUp() {

        ///KUNDE///
        kunde.setVorname("Aaron");
        kunde.setNachname("Hari");
        kunde.setAlter(16);
        kunde.setGeschlecht("Mänlich");

        Adresse adresse = new Adresse();
        adresse.setStrassenname("Tollestrasse");
        adresse.setStrassennumer("49a");
        adresse.setOrt("Interlaken");
        adresse.setPostleitzahl(3800);
        kunde.setAdresse(adresse);
        ///ENDE///

        ///ARTIKEL///
        Artikel artikelBuch = new Artikel();
        artikelBuch.setGewicht(327);
        artikelBuch.setName("Wilhelm Tell");
        artikelBuch.setPreis(13.9);

        Artikel artikelLampe = new Artikel();
        artikelLampe.setGewicht(190);
        artikelLampe.setName("Smart Bulb");
        artikelLampe.setPreis(39.95);
        ///ENDE///

        ///BESTELLTER ARTIKEL///
        BestellterArtikel bestellterArtikel1 = new BestellterArtikel();
        bestellterArtikel1.setAnzahl(3);
        bestellterArtikel1.setArtikel(artikelLampe);

        BestellterArtikel bestellterArtikel2 = new BestellterArtikel();
        bestellterArtikel2.setAnzahl(1);
        bestellterArtikel2.setArtikel(artikelBuch);

        BestellterArtikel bestellterArtikel3 = new BestellterArtikel();
        bestellterArtikel3.setAnzahl(20);
        bestellterArtikel3.setArtikel(artikelBuch);
        ///ENDE///

        ///ZAHLUNGS MITTEL///
        Zahlungsmittel zahlung1 = new Zahlungsmittel();
        zahlung1.setName("Master Card");
        zahlung1.setBetrag(19.95);

        Zahlungsmittel zahlung2 = new Zahlungsmittel();
        zahlung2.setName("Bargeld CHF");
        zahlung2.setBetrag(0.05);

        Zahlungsmittel zahlung3 = new Zahlungsmittel();
        zahlung3.setName("Bargeld CHF");
        zahlung3.setBetrag(10000.0);
        ///ENDE///

        ///BESTELLUNG///
        Bestellung bestellung1 = new Bestellung();
        bestellung1.setDatum("13.09.2020");
        bestellung1.addArtikel(bestellterArtikel1);
        bestellung1.addArtikel(bestellterArtikel2);
        bestellung1.addZahlung(zahlung1);
        bestellung1.addZahlung(zahlung2);
        kunde.addBestellung(bestellung1);

        Bestellung bestellung2 = new Bestellung();
        bestellung2.setDatum("07.09.2020");
        bestellung2.addArtikel(bestellterArtikel2);
        bestellung2.addArtikel(bestellterArtikel3);
        bestellung2.addZahlung(zahlung1);
        bestellung2.addZahlung(zahlung3);
        kunde.addBestellung(bestellung2);

        Bestellung bestellung3 = new Bestellung();
        bestellung3.setDatum("07.09.2020");
        bestellung3.addArtikel(bestellterArtikel1);
        bestellung3.addZahlung(zahlung1);
        kunde.addBestellung(bestellung3);
        ///ENDE///
    }

    @Test
    void zeigeBezahlteBestellungen() {
        System.out.println("//////////zeige Bezahlte Bestellungen//////////");
        kunde.zeigeBezahlteBestellungen();
    }

    @Test
    void zeigeLetzteBestellung() {
        System.out.println("//////////zeige Letzte Bestellung//////////");
        kunde.zeigeLetzteBestellung();
    }

    @Test
    void zeigeOffeneBetraege() {
        System.out.println("//////////zeige offene Beträge//////////");
        kunde.zeigeOffeneBetraege();
    }

    @Test
    void zeigeBestellung() {
        System.out.println("//////////zeige Bestellung(index)//////////");
        kunde.zeigeBestellung(0);
    }

    @Test
    void erstelleEtikete() {
        System.out.println("//////////erstelle Etikete//////////");
        kunde.erstelleEtikete();
    }
}