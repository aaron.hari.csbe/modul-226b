package repetition226a;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FirmaTest {
    Firma firma;

    @BeforeEach
    void setUp() {
        firma = new Firma();
        firma.addNewKunde("Aaron", "Hari");
    }

    @Test
    void getKunde() {
        Kunde kunde = firma.getKunde(0);
        assertEquals("Aaron", kunde.vorname);
        assertEquals("Hari", kunde.nachname);
    }

    @Test
    void setKunde() {
        firma.setKunde(0, "Daniel", "Schmitz");
        Kunde kunde = firma.getKunde(0);
        assertEquals("Daniel", kunde.vorname);
        assertEquals("Schmitz", kunde.nachname);
    }

    @Test
    void addNewKunde() {
        firma.addNewKunde("Daniel", "Schmitz");
        Kunde kunde = firma.getKunde(1);
        assertEquals("Daniel", kunde.vorname);
        assertEquals("Schmitz", kunde.nachname);
    }

    @Test
    void addKunde() {
        Kunde kunde = new Kunde("Daniel", "Schmitz");
        firma.addKunde(kunde);
        Kunde newKunde = firma.getKunde(1);
        assertEquals("Daniel", newKunde.vorname);
        assertEquals("Schmitz", newKunde.nachname);

    }

    @Test
    void delKunde() {
        int sizeBefore = firma.kunden.size();
        firma.delKunde("Hari");
        int sizeAfter = firma.kunden.size();
        assertEquals(sizeAfter + 1, sizeBefore);
    }
}